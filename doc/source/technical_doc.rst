Technical documentation
=======================

.. automodule:: main
.. autoclass:: SelectableLabel
    :members:
    :private-members:
    :special-members:
.. autoclass:: Commute
    :members:
    :private-members:
    :special-members:
.. autoclass:: CommuteApp
    :members:
    :private-members:
    :special-members:
    
.. automodule:: commute_db
.. autoclass:: KedDB
    :members:
    :private-members:
    :special-members:
