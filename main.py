# -*- coding: UTF8 -*-
"""
    KEDy.main
    ~~~~~~~~~

    The application allows you to calculate the impact of your daily trips
    on the environment and your expenses.

    :copyright: (c) 2017 par Moulinux.
    :license: GPLv3, see LICENSE for more details.
"""
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.uix.modalview import ModalView
from kivy.uix.button import Button
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.label import Label
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from commute_db import KedDB


def makedata(text, selected=False):
    return {'text': text, 'selected': selected}


class SelectableRecycleBoxLayout(FocusBehavior, LayoutSelectionBehavior,
                                 RecycleBoxLayout):
    ''' Adds selection and focus behaviour to the view. '''


class SelectableLabel(RecycleDataViewBehavior, Label):
    ''' Add selection support to the Label '''
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(SelectableLabel, self).refresh_view_attrs(
            rv, index, data)

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(SelectableLabel, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected
        # !important! : index validation
        # if you remove raw data entry, The value of index arg be less than
        # the value of self.index. Or the value of index arg indicates out
        # of bounds of 'data' array.
        # note: after leave this function, refresh_view_attrs() are called.
        # so you should be re-assign self.index.
        if (self.index == index) and (index < len(rv.data)):
            rv.data[index]['selected'] = is_selected


class Commute(BoxLayout):
    ''' Controler class of the main screen '''

    # fields
    distance_label = ObjectProperty()
    distance_slider = ObjectProperty()
    transport_label = ObjectProperty()
    transport_slider = ObjectProperty()
    err_label = ObjectProperty()
    dep_list = ObjectProperty()

    def __init__(self, db, dico, **kwargs):
        super(Commute, self).__init__(**kwargs)

        # Init database object and dico
        self.db = db
        self.dico = dico
        self.text = dico['choice']
        self.distance = 0
        self.transport = 0

        # Fill the RV
        data = self.ids.rv.data
        commutes = self.db.list_commutes()
        for c in commutes:
            # Format text
            comm = self.text.format(c['distance'],
                                    self.dico[c['transport']],
                                    c['id'])
            # Add the commute to the RV
            data.append(makedata(comm))

        # Configure the distance instance
        self.distance_label.text = self.dico['distance'].format(0)
        self.distance_slider.bind(value=self.update_distance)

        # Configure the transport instance
        self.transport_label.text = self.dico['transport'].format(self.dico[0])
        self.transport_slider.bind(value=self.update_transport)

    def update_distance(self, instance, value):
        ''' Update the distance label '''
        self.distance = int(value)
        self.distance_label.text = self.dico['distance'].format(int(value))
        if self.distance != 0:
            self.err_label.text = ''

    def update_transport(self, instance, value):
        ''' Update the transport label '''
        self.transport = int(value)
        self.transport_label.text = self.dico['transport'].\
            format(self.dico[int(value)])
        if self.transport != 0:
            self.err_label.text = ''

    def add_dep(self):
        ''' Add item to the view '''

        if self.distance != 0 and self.transport != 0:
            # Add the commute to database
            self.db.insert_commute(self.distance, self.transport)

            # Get the distance and transport inputs
            comm = self.text.format(self.distance,
                                    self.dico[self.transport],
                                    self.db.lastrowid())

            # Add the dep to rv
            data = self.ids.rv.data
            data.insert(0, makedata(comm))

        else:
            self.err_label.text = self.dico['err_input']

    def remove_dep(self, *args):
        ''' Remove all selected data '''

        rv = self.ids.rv
        lm = rv.layout_manager

        # remove raw data
        tmp_data = []
        for i, v in enumerate(rv.data):
            if not v['selected']:
                # Add to tmp list
                tmp_data.append(v)
            else:
                # Remove from database
                self.db.delete_commute(v['text'].split('#')[1])

        rv.data = tmp_data
        # clear all selection state
        lm.clear_selection()

    def update_dep(self, *args):
        ''' Update all selected items '''

        # update raw data
        data = self.ids.rv.data

        for v in data:
            if v['selected']:

                if self.distance != 0 and self.transport != 0:
                    # Update database
                    self.db.update_commute(v['text'].split('#')[1],
                                           self.distance,
                                           self.transport)
                    # Format commute
                    comm = self.text.format(self.distance,
                                            self.dico[self.transport],
                                            v['text'].split('#')[1])
                    # Update the matching item
                    v['text'] = comm
                else:
                    self.err_label.text = self.dico['err_input']

        # refresh visible items
        self.ids.rv.refresh_from_data()
        lm = self.ids.rv.layout_manager
        lm.clear_selection()


class CommuteApp(App):
    ''' Main class '''

    def __init__(self, **kwargs):
        super(CommuteApp, self).__init__(**kwargs)

        # init dictionary
        import dict_en
        self.dico = dict_en.dico

        # Create database object
        import create_db
        self.db = KedDB(self.dico)

    def build(self):
        self.title = self.dico['title']
        return Commute(self.db, self.dico)

    def open_about(self, *largs):
        view = ModalView()

        # create content and add it to the view
        content = Button(text=self.dico['txt_about'])
        view = ModalView(auto_dismiss=False)
        view.add_widget(content)

        # bind the on_press event of the button to the dismiss function
        content.bind(on_press=view.dismiss)
        view.open()


if __name__ == '__main__':
    CommuteApp().run()
