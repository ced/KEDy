# -*- coding: UTF8 -*-
"""
    KEDy.commute_db
    ~~~~~~~~~~~~~~~

    The module implements the management of the Sqlite database.

    :copyright: (c) 2017 par Moulinux.
    :license: GPLv3, see LICENSE for more details.
"""
import sqlite3
import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
basedir = os.path.abspath(os.path.dirname(__file__))


class KedDB:
    ''' Setting up and interfacing a SQLite database '''

    def __init__(self, dico):
        ''' Establishing the connection - Creating the cursor '''

        self.dico = dico

        try:
            self.conn = sqlite3.connect(
                os.path.join(basedir, 'KEDy.db'), check_same_thread=False,
                detect_types=sqlite3.PARSE_DECLTYPES)
        except sqlite3.DatabaseError as err:
            print(self.dico['DbInitError'].format(err))
            self.fail = True
        else:
            self.conn.row_factory = sqlite3.Row
            self.conn.isolation_level = None    # autocommit
            self.cursor = self.conn.cursor()    # create cursor
            self.cursor.execute("PRAGMA foreign_keys=ON")
            self.fail = False

    def execute_req(self, req, params):
        '''
            Execution of the request, with possible error detection

            :param req: SQL query
            :type req: str

            :param params: request parameters
            :type params: tuple

            :return: True if the execute was successful, False if not
            :rtype: bool
        '''
        try:
            self.cursor.execute(req, params)
        except sqlite3.DatabaseError as err:
            # display the request and the system error message :
            print(self.dico['DbSqlError'].format(req, err))
            return False
        else:
            return True

    def result_req(self):
        ''' Returns the result of the previous query (a sqlite3.Row object) '''
        return self.cursor.fetchall()

    def commit(self):
        ''' Transfer of the cursor to the disk '''
        if self.conn:
            self.conn.commit()

    def close(self):
        ''' Closing the cursor '''
        if self.conn:
            self.cursor.close()
            self.conn.close()

    def lastrowid(self):
        '''
            Returns the last id created

            :return: last id of commute created
            :rtype: int
        '''
        return self.cursor.lastrowid

    def insert_commute(self, dist, trans):
        '''
            Add a new commute.

            :param dist: distance of the commute
            :type dist: int

            :param trans: mode of transport for the commute
            :type trans: int

            :return: True if the insert was successful, False if not
            :rtype: bool
        '''
        sql = "INSERT INTO commutes(distance, transport) VALUES (?,?)"
        test = self.execute_req(sql, (dist, trans))
        return test

    def list_commutes(self):
        '''
            List all the commutes

            :return: result set of the commutes
            :rtype: sqlite3.Row object
        '''
        sql = "SELECT id, distance, transport FROM commutes"
        if self.execute_req(sql, ()):
            return self.result_req()

    def update_commute(self, id, dist, trans):
        '''
            Updates a commute

            :param id: id of the commute
            :type d: int

            :param dist: distance of the commute
            :type dist: int

            :param trans: mode of transport for the commute
            :type trans: int

            :return: True if the update was successful, False if not
            :rtype: bool
        '''
        sql = """UPDATE commutes SET distance=?, transport=?
                    WHERE id=?"""
        test = self.execute_req(sql, (dist, trans, id))
        return test

    def delete_commute(self, id):
        '''
            Deletes a commute

            :param id: id of the commute
            :type d: int

            :return: True if the delete was successful, False if not
            :rtype: bool
        '''
        sql = "DELETE FROM commutes WHERE id=?"
        test = self.execute_req(sql, (id,))
        return test
