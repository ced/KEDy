# -*- coding: UTF8 -*-
dico = {
    'title': "Enregistrement des déplacements quotidiens",
    'distance': "Distance = {}",
    'transport': "Mode de transport = {}",
    'add': "Ajouter",
    'remove': "Supprimer",
    'update': "Remplacer",
    'view': "Détail",
    'choice': "J'ai fait {} km en {} #{}",
    'DbInitError':
    """La connexion avec la base de données a échoué :
    Erreur détectée :{}""",
    'DbSqlError':
    """Requête SQL incorrecte :{}
    Erreur détectée :{}""",
    'review': "Bilan annuel",
    'about': "À propos",
    'settings': "Configuration",
    0: "** à saisir **",
    1: "Vélo",
    2: 'train',
    3: "voiture",
    'txt_about':
    """
    Cette application permet de calculer l'impact de des
    déplacements quotidiens sur l'environnement et les dépenses.

    Elle gère les enregistrements des différents déplacements,
    ainsi que leur bilan sur l'année.

    Cette application a été écrite par Stéphane Moulinet.
    Elle est disponible selon les termes de la licence GPLv3.

    APPUYEZ SUR L'ÉCRAN, POUR SORTIR...
    """,
    'err_input': "Veillez à bien configurer votre déplacement...",
}
