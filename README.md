# KEDy : Recording of daily commutes

The application records daily commutes between home and work.
![Screenshot](/img/screenshot.png)

## Introduction
The application is inspired by the [Ademe] eco-displacement calculator.
In addition to being an application implementing the kivy framework,
it raises awareness of the impact of commuting on the environment and expenses.

This development is in the continuation of the progression of my course
materials dedicated python language through the application CEDy
(last iteration on [command line][cl], with the [graphics module Tkinter][tkinter])

## ToDo list
* Detail view (showing all costs of a selected commute or even better comparing costs)
* Annual review of the impact of commuting on the environment and expenses
* Config menu at the first app lauching (when database is empty)
* Read the costs (environment, expenses) through an API
* Export the commuting survey through an API

> The application is not yet ready for use in the field, for awareness surveys.
[Ademe] data will need to be updated and extended to other transport modes.

## Technical environment
On a PC, once your [kivy environment][kivy] is configured, all you have to do is
execute the following command : `python main.py`

The application can be easily packaged for an android system using buildozer.
Once your [buildozer environment][buildozer] is configured, all you have to do is
execute the following command : `buildozer android debug`

> The project is configured for `x86_64` arch. You probably have to change this.

## Technical documentation
The KEDy application has a technical documentation that was generated with Sphinx.
It is accessible [online][doc]
(via the framagit pages and continuous integration).


[Ademe]: https://www.ademe.fr/
[cl]: https://framagit.org/ced/CEDy/5-gerer-les-erreurs
[tkinter]: https://framagit.org/ced/CEDy/7-GUI-Tkinter
[doc]: http://ced.frama.io/KEDy
[kivy]: https://kivy.org/doc/stable/gettingstarted/installation.html
[buildozer]: https://kivy.org/doc/stable/guide/packaging-android.html
