from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["kivy"],
                     "include_files": ["dict_en.py", "dict_fr.py", "img",
                                       "commute.kv", "create_db.py",
                                       "commute_db.py"], }

# we call the setup function
setup(
    name="KEDy",
    version="0.1",
    description="Recording of daily commutes",
    options={"build_exe": build_exe_options},
    executables=[Executable("main.py")],
)
