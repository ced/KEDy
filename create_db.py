# -*- coding: UTF8 -*-
"""
    KEDy.create_db
    ~~~~~~~~~~~~~~

    The module creates the database file.

    :copyright: (c) 2017 par Moulinux.
    :license: GPLv3, see LICENSE for more details.
"""
import sqlite3
import sys
import os

SQL = """
CREATE TABLE transports
(id INTEGER NOT NULL,
libelle VARCHAR(10) NOT NULL,
CONSTRAINT pk_transports PRIMARY KEY (id));

CREATE TABLE commutes
(id INTEGER NOT NULL,
distance INTEGER NOT NULL,
transport INTEGER NOT NULL,
CONSTRAINT pk_deplacements PRIMARY KEY (id),
CONSTRAINT fk_deplacements_1 FOREIGN KEY(transport) REFERENCES transports(id));

INSERT INTO transports VALUES
(1, "vélo"),
(2, "train"),
(3, "voiture");
"""

if not os.path.exists('KEDy.db'):
    with open('KEDy.db', 'w'):
        try:
            # Create the database file in the current folder :
            bdd = sqlite3.connect('KEDy.db')
            curseur = bdd.cursor()

            # Execute the SQL script :
            curseur.executescript(SQL)

            bdd.commit()
        except sqlite3.DatabaseError as err:
            print("Erreur {}".format(err))
            sys.exit(1)
        finally:
            if bdd:
                curseur.close()
                bdd.close()
else:
    print(u"The {} file already exist".format('KEDy.db'))
