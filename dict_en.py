# -*- coding: UTF8 -*-
dico = {
    'title': "Recording of daily commutes",
    'distance': "Distance = {}",
    'transport': "Mode of transport = {}",
    'add': "Add",
    'remove': "Remove",
    'update': "Update",
    'view': "View",
    'choice': "My daily commute of {} km is made by {} #{}",
    'DbInitError':
    """The connection to the database has failed :
    Error detected :{}""",
    'DbSqlError':
    """Incorrect SQL query :{}
    Error detected :{}""",
    'review': "Annual review",
    'about': "About",
    'settings': "Settings",
    0: "** to be filled **",
    1: "bike",
    2: 'train',
    3: "car",
    'txt_about':
    """
    This application allows you to calculate the impact of daily commuting
    on the environment and expenses.

    It manages the records of various commutes, as well as their annual review.

    This application was written by Stéphane Moulinet.
    It is available under the terms of the GPLv3 license.

    TAP THE SCREEN, TO EXIT....
    """,
    'err_input': "Make sure you the commute is configured correctly...",
}
